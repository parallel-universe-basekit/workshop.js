require.config({
	// the base url for app paths in require config
	baseUrl: 'app',
	// tell me where the assets are or your dependancies will be rendered useless!
	paths: {
		'jquery': '../bower_components/jquery/dist/jquery',
		'jquery-noconflict': '../scripts/jquery-noconflict'
	},
	// map dependancies, we can substitute shit here! Awesome.
	map: {
		'*': {'jquery':'jquery-noconflict'},
		'jquery-noconflict': { 'jquery': 'jquery'}
	}
});

// entry point for our modules, yay
define(['app'],function(app) {

});